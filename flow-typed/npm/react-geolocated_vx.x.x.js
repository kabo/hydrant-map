// flow-typed signature: abc69ffd8965aa69ae402337ecb11d09
// flow-typed version: <<STUB>>/react-geolocated_v2.4.0/flow_v0.73.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-geolocated'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-geolocated' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'react-geolocated/dist-modules/components/geolocated' {
  declare module.exports: any;
}

declare module 'react-geolocated/dist-modules/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'react-geolocated/dist-modules/components/geolocated.js' {
  declare module.exports: $Exports<'react-geolocated/dist-modules/components/geolocated'>;
}
declare module 'react-geolocated/dist-modules/index.js' {
  declare module.exports: $Exports<'react-geolocated/dist-modules/index'>;
}
