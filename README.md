# Hydrant map

[![pipeline status](https://gitlab.com/kabo/hydrant-map/badges/master/pipeline.svg)](https://gitlab.com/kabo/hydrant-map/commits/master)

A map showing fire hydrants and emergency water tanks in Raglan, New Zealand, using open street map tiles and data. The map is overlaid with a voronoi diagram as well.

If you'd like to use this for some other area, just clone the repo and edit `src/config.json` to set your own `maxBounds` and default `viewport`.

## Changelog

### v0.5.0 (2018-06-02)

 - Added image overlay from PDFs

### v0.4.3 (2018-06-03)

 - Added test date field

### v0.4.2 (2018-06-03)

 - Added loading message

### v0.4.1 (2018-06-02)

 - Optimised favicon

### v0.4.0 (2018-06-02)

 - Colour coded hydrants based on working condition. Non-working hydrants aren't included in the voronoi overlay.
 - Increased maxZoom from 18 to 19
 - Can click hydrant to get more info in a popup

### v0.3.2 (2018-05-28)

 - Fixes #2

### v0.3.1 (2018-05-28)

 - Highlight the cell that one is currently in

### v0.3.0 (2018-05-28)

 - Added GeoLocation

### v0.2.0 (2018-05-28)

 - Added [LINZ Aerial Photos](https://data.linz.govt.nz/layer/51872-waikato-05m-rural-aerial-photos-2012-2013/webservices/) tiles

### v0.1.0 (2018-05-27)

 - First release

## License

Released under [COIL 0.5](./LICENSE.md) ([Copyfree Open Innovation License](http://coil.apotheon.org/)).

Project icon: [Adopt A Hydrant](https://thenounproject.com/search/?q=fire%20hydrant&i=14303) by Iconathon from the Noun Project
