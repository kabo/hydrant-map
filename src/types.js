// @flow

export type Hydrant = {
  +id: number,
  +lon: number,
  +lat: number,
  +type: string,
  +working: string,
  +testDate: string,
  +comment: string,
  +position: string,
}
export type Hydrants = Array<Hydrant>
export type Viewport = {
  +center: [number, number],
  +zoom: number,
}
export type State = {
  +hydrants: Hydrants,
  +viewport: Viewport,
}
export type OverpassNode = {
  id: number,
  lat: number,
  lon: number,
  type: string,
  tags: {
    emergency: string,
    'fire_hydrant:position': string,
    'fire_hydrant:type': string,
    'working:test_date': string,
    working: string,
    description: string,
  }
}
export type OverpassResponse = {
  generator: string,
  version: number,
  osm3s: {
    copyright: string,
    timestamp_osm_base: string,
  },
  elements: Array<OverpassNode>,
}
export type LoadHydrantsDoneAction = { type: 'LOAD_HYDRANTS_DONE', payload: OverpassResponse }
export type LoadHydrantsFailedAction = { type: 'LOAD_HYDRANTS_FAILED', payload: string }
export type LoadHydrantsAction = {
  type: 'LOAD_HYDRANTS_REQUEST',
  meta: {
    offline: {
      effect: { url: string  },
      commit: { type: 'LOAD_HYDRANTS_DONE' },
      rollback: { type: 'LOAD_HYDRANTS_FAILED' },
    }
  }
}
export type SetViewportAction = { type: 'SET_VIEWPORT', viewport: Viewport }

export type Actions = LoadHydrantsDoneAction | LoadHydrantsAction | LoadHydrantsFailedAction | SetViewportAction
