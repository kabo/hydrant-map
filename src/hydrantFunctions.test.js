// @flow

import { niceDate, HYDRANT_WORKING_TESTDATE_UNKNOWN, isWorkingHydrant, HYDRANT_COLOR_WORKING, HYDRANT_COLOR_NOT_WORKING, HYDRANT_COLOR_KINDOF_WORKING, HYDRANT_COLOR_UNKNOWN, hydrantColor, HYDRANT_TYPE_HYDRANT, HYDRANT_TYPE_TANK, HYDRANT_TYPE_UNKNOWN, niceType, HYDRANT_POSITION_UNKNOWN, HYDRANT_POSITION_GREEN, HYDRANT_POSITION_LANE, HYDRANT_POSITION_PARKINGLOT, HYDRANT_POSITION_SIDEWALK, nicePosition, HYDRANT_WORKING_NO, HYDRANT_WORKING_YES, HYDRANT_WORKING_UNKNOWN, HYDRANT_WORKING_KINDOF, niceWorking } from './hydrantFunctions'

const mockHydrant = {
  id: 1,
  lon: 2,
  lat: 3,
  comment: 'no comment',
  type: 'fire_hydrant',
  position: 'green',
  testDate: '2018-01-01',
}

describe('hydrantFunctions', () => {
  describe('isWorkingHydrant', () => {
    it('returns true when working is "yes"', () => {
      expect(isWorkingHydrant({ ...mockHydrant, working: 'yes' })).toBe(true)
    })
    it('returns true when working is undefined', () => {
      expect(isWorkingHydrant({ ...mockHydrant, working: undefined })).toBe(true)
    })
    it('returns false when working is no', () => {
      expect(isWorkingHydrant({ ...mockHydrant, working: 'no' })).toBe(false)
    })
    it('returns false when working is kindof', () => {
      expect(isWorkingHydrant({ ...mockHydrant, working: 'kindof' })).toBe(false)
    })
  })
  describe('hydrantColor', () => {
    it('returns working color when working is "yes"', () => {
      expect(hydrantColor({ ...mockHydrant, working: 'yes' })).toBe(HYDRANT_COLOR_WORKING)
    })
    it('returns unknown color when working is undefined', () => {
      expect(hydrantColor({ ...mockHydrant, working: undefined })).toBe(HYDRANT_COLOR_UNKNOWN)
    })
    it('returns non working color when working is no', () => {
      expect(hydrantColor({ ...mockHydrant, working: 'no' })).toBe(HYDRANT_COLOR_NOT_WORKING)
    })
    it('returns kindof color when working is kindof', () => {
      expect(hydrantColor({ ...mockHydrant, working: 'kindof' })).toBe(HYDRANT_COLOR_KINDOF_WORKING)
    })
  })
  describe('niceType', () => {
    it('correct fire hydrant', () => {
      expect(niceType('fire_hydrant')).toBe(HYDRANT_TYPE_HYDRANT)
    })
    it('correct water tank', () => {
      expect(niceType('water_tank')).toBe(HYDRANT_TYPE_TANK)
    })
    it('correct random string', () => {
      expect(niceType('random')).toBe('random')
    })
    it('correct undefined', () => {
      expect(niceType()).toBe(HYDRANT_TYPE_UNKNOWN)
    })
  })
  describe('nicePosition', () => {
    it('correct green', () => {
      expect(nicePosition('green')).toBe(HYDRANT_POSITION_GREEN)
    })
    it('correct lane', () => {
      expect(nicePosition('lane')).toBe(HYDRANT_POSITION_LANE)
    })
    it('correct sidewalk', () => {
      expect(nicePosition('sidewalk')).toBe(HYDRANT_POSITION_SIDEWALK)
    })
    it('correct parking_lot', () => {
      expect(nicePosition('parking_lot')).toBe(HYDRANT_POSITION_PARKINGLOT)
    })
    it('correct random', () => {
      expect(nicePosition('random')).toBe('random')
    })
    it('correct undefined', () => {
      expect(nicePosition()).toBe(HYDRANT_POSITION_UNKNOWN)
    })
  })
  describe('niceWorking', () => {
    it('correct yes', () => {
      expect(niceWorking('yes')).toBe(HYDRANT_WORKING_YES)
    })
    it('correct no', () => {
      expect(niceWorking('no')).toBe(HYDRANT_WORKING_NO)
    })
    it('correct kindof', () => {
      expect(niceWorking('kindof')).toBe(HYDRANT_WORKING_KINDOF)
    })
    it('correct random', () => {
      expect(niceWorking('random')).toBe('random')
    })
    it('correct undefined', () => {
      expect(niceWorking()).toBe(HYDRANT_WORKING_UNKNOWN)
    })
  })
  describe('niceDate', () => {
    it('correct date', () => {
      expect(niceDate('2018-01-01')).toBe('Jan 01, 2018')
    })
    it('correct undefined', () => {
      expect(niceDate()).toBe(HYDRANT_WORKING_TESTDATE_UNKNOWN)
    })
  })
})

