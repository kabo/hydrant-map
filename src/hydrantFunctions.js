// @flow

import dayjs from 'dayjs'
import type { Hydrant } from './types'

export const HYDRANT_COLOR_WORKING = '#00FF00'
export const HYDRANT_COLOR_NOT_WORKING = '#666666'
export const HYDRANT_COLOR_KINDOF_WORKING = '#CC9900'
export const HYDRANT_COLOR_UNKNOWN = '#3388FF'
export const HYDRANT_TYPE_HYDRANT = 'Fire hydrant'
export const HYDRANT_TYPE_TANK = 'Water tank'
export const HYDRANT_TYPE_UNKNOWN = '?'
export const HYDRANT_POSITION_UNKNOWN = '?'
export const HYDRANT_POSITION_GREEN = 'In the green'
export const HYDRANT_POSITION_LANE = 'Lane'
export const HYDRANT_POSITION_PARKINGLOT = 'Parking lot'
export const HYDRANT_POSITION_SIDEWALK = 'Sidewalk'
export const HYDRANT_WORKING_NO = 'No'
export const HYDRANT_WORKING_YES = 'Yes'
export const HYDRANT_WORKING_UNKNOWN = '?'
export const HYDRANT_WORKING_KINDOF = 'Kind of'
export const HYDRANT_WORKING_TESTDATE_UNKNOWN = '?'

export const isWorkingHydrant = (hydrant: Hydrant): boolean => hydrant.working === 'yes' || hydrant.working === undefined
export const hydrantColor = (hydrant: Hydrant): string => {
  switch (hydrant.working) {
    case 'yes':
      return HYDRANT_COLOR_WORKING
    case 'no':
      return HYDRANT_COLOR_NOT_WORKING
    case 'kindof':
      return HYDRANT_COLOR_KINDOF_WORKING
    default:
      return HYDRANT_COLOR_UNKNOWN
  }
}
export const niceType = (type?: string): string => {
  switch (type) {
    case 'fire_hydrant': return HYDRANT_TYPE_HYDRANT
    case 'water_tank'  : return HYDRANT_TYPE_TANK
    default            : return type === undefined ? HYDRANT_TYPE_UNKNOWN : type
  }
}
export const nicePosition = (position?: string): string => {
  switch (position) {
    case 'green'      : return HYDRANT_POSITION_GREEN
    case 'lane'       : return HYDRANT_POSITION_LANE
    case 'sidewalk'   : return HYDRANT_POSITION_SIDEWALK
    case 'parking_lot': return HYDRANT_POSITION_PARKINGLOT
    default           : return position === undefined ? HYDRANT_POSITION_UNKNOWN : position
  }
}
export const niceWorking = (working?: string): string => {
  switch (working) {
    case 'yes'    : return HYDRANT_WORKING_YES
    case 'no'     : return HYDRANT_WORKING_NO
    case 'kindof' : return HYDRANT_WORKING_KINDOF
    default       : return working === undefined ? HYDRANT_WORKING_UNKNOWN : working
  }
}
export const niceDate = (date?: string): string => {
  if (!date) { // flowlint-line sketchy-null:off
    // it doesn't matter if it's an empty string or null or undefined
    // we want to return HYDRANT_WORKING_TESTDATE_UNKNOWN in all those cases anyway
    return HYDRANT_WORKING_TESTDATE_UNKNOWN
  }
  return dayjs(date).format('MMM DD, YYYY')
}


