// @flow

import { minZoom } from './config.json' // flowlint-line untyped-import:off
import type { Viewport, SetViewportAction, LoadHydrantsAction  } from './types'

export function loadHydrants(bbox: string): LoadHydrantsAction {
  const data = `[out:json];(node[emergency=fire_hydrant](${bbox});node[emergency=water_tank](${bbox}););out qt;`
  const options = {
    method: 'POST',
    headers: { 'content-type': 'application/x-www-form-urlencoded; charset=UTF-8' },
    body: `data=${encodeURIComponent(data)}`.replace('%20', '+')
  }
  return {
    type: 'LOAD_HYDRANTS_REQUEST',
    meta: {
      offline: {
        effect: { url: 'https://overpass-api.de/api/interpreter', ...options },
        commit: { type: 'LOAD_HYDRANTS_DONE' },
        rollback: { type: 'LOAD_HYDRANTS_FAILED' },
      }
    }
  }
}
export function setViewport(viewport: Viewport): SetViewportAction {
  if (viewport.zoom < minZoom) {
    viewport = { ...viewport, zoom: minZoom }
  }
  return {
    type: 'SET_VIEWPORT',
    viewport
  }
}
