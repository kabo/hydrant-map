// @flow

/* eslint no-unused-vars: "off" */

import { Logger } from 'aws-amplify'
import type { State, Actions, LoadHydrantsDoneAction, SetViewportAction } from './types'
import { viewport } from './config.json' // flowlint-line untyped-import:off

const logger = new Logger('reducer', 'INFO')
const defaultState = {
  hydrants: [],
  viewport
}

const actions = {
  'LOAD_HYDRANTS_DONE': (state: State, action: LoadHydrantsDoneAction) => {
    return { ...state, hydrants: action.payload.elements.map(n => ({
      id: n.id,
      lon: n.lon,
      lat: n.lat,
      type: n.tags.emergency,
      working: n.tags.working,
      comment: n.tags.description,
      position: n.tags['fire_hydrant:position'],
      testDate: n.tags['working:test_date'],
    })) }
  },
  'SET_VIEWPORT': (state: State, action: SetViewportAction) => {
    return { ...state, viewport: action.viewport }
  },
}

const reducer = function(state: State = defaultState, action: Actions): State {
  // logger.info(action)
  switch (action.type) {
    case 'LOAD_HYDRANTS_DONE':
      return actions[action.type](state, action)
    case 'SET_VIEWPORT':
      return actions[action.type](state, action)
    default:
      return state
  }
}

export default reducer
