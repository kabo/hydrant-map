// @flow

/* eslint no-unused-vars: "off" */

import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Map, LayersControl, FeatureGroup, LayerGroup, ImageOverlay, TileLayer, CircleMarker, Popup, GeoJSON } from 'react-leaflet'
import * as th from '@turf/helpers'
import voronoi from '@turf/voronoi'
import booleanPointInPolygon from '@turf/boolean-point-in-polygon'
import { Logger } from 'aws-amplify'
import {geolocated} from 'react-geolocated'
import type { Viewport, SetViewportAction, Hydrants, LoadHydrantsAction, Actions } from './types'
import { setViewport, loadHydrants } from './actions'
import { isWorkingHydrant, hydrantColor, niceType, nicePosition, niceWorking, niceDate } from './hydrantFunctions'
import { maxBounds } from './config.json' // flowlint-line untyped-import:off
import { linzKey } from './secrets.json' // flowlint-line untyped-import:off

const logger = new Logger('App', 'INFO')
const { BaseLayer, Overlay } = LayersControl

type Props = {
  isGeolocationAvailable: boolean,
  isGeolocationEnabled: boolean,
  coords: {
    latitude: number,
    longitude: number,
    altitude: number,
    heading: number,
    speed: number,
    accuracy: number,
    altitudeAccuracy: number
  },
  hydrants: Hydrants,
  viewport: Viewport,
  loadHydrants: (bbox: string) => LoadHydrantsAction,
  setViewport: (viewport: Viewport) => SetViewportAction,
  //addHydrant: () => any, // used for debugging changes to hydrants
}

class App extends Component<Props> {
  componentDidMount() {
    // give redux-offline some time, avoids double requests
    setTimeout(() => this.props.loadHydrants(`${maxBounds.S},${maxBounds.W},${maxBounds.N},${maxBounds.E}`), 500)
    //setTimeout(() => this.props.addHydrant(), 2000)
  }
  render() {
    const mapMaxBounds = [
      [maxBounds.S, maxBounds.W],
      [maxBounds.N, maxBounds.E]
    ]
    const { viewport, hydrants, coords } = this.props
    if (!viewport || !hydrants || hydrants.length < 1) {
      return <div className="App"></div>
    }
    const currentPoint = coords && th.point([coords.longitude, coords.latitude])
    const mapViewport = { ...viewport }
    const hydrantCollection = th.featureCollection(hydrants.filter(isWorkingHydrant).map(h => th.point([h.lon, h.lat])))
    const voronoiPolygons = voronoi(hydrantCollection, {bbox: [maxBounds.W, maxBounds.S, maxBounds.E, maxBounds.N]})
    const voronoiStyle = (feature) => {
      if (currentPoint && booleanPointInPolygon(currentPoint, feature)) {
        return {
          color: '#F00',
          weight: 4,
          opacity: 0.3,
          fill: true,
          fillColor: '#F00',
          fillOpacity: 0.2,
        }
      }
      return {
        color: '#000',
        weight: 1,
        opacity: 0.3,
        fill: false,
      }
    }
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Hydrant Map</h1>
        </header>
        <div className="map-wrapper">
          <Map
            viewport={mapViewport}
            maxBounds={mapMaxBounds}
            onViewportChanged={this.props.setViewport}
            style={{height: '100%'}}
          >
            <LayersControl position="topleft">
              <BaseLayer checked name="OpenStreetMap">
                <TileLayer
                  attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors | <a href=&quot;https://gitlab.com/kabo/hydrant-map&quot;>Source</a> | <a href=&quot;https://www.openstreetmap.org/user/new&quot;>Contribute</a>"
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  maxZoom={19}
                />
              </BaseLayer>
              <BaseLayer name="LINZ">
                <TileLayer
                  attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors | <a href=&quot;https://data.linz.govt.nz/layer/93652-nz-10m-satellite-imagery-2017/webservices/&quot;>LINZ</a> | <a href=&quot;https://gitlab.com/kabo/hydrant-map&quot;>Source</a> | <a href=&quot;https://www.openstreetmap.org/user/new&quot;>Contribute</a>"
                  url={`http://tiles-a.data-cdn.linz.govt.nz/services;key=${linzKey}/tiles/v4/layer=51872/EPSG:3857/{z}/{x}/{y}.png`}
                  maxZoom={21}
                  maxNativeZoom={18}
                />
              </BaseLayer>
              <Overlay checked name="PDF Map">
                <LayerGroup>
                  <ImageOverlay
                    url="imageoverlay/A1.gif"
                    bounds={[[-37.807671170980086,174.84055808553182],[-37.80311411430391,174.84910182767814]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/B1.gif"
                    bounds={[[-37.811686295976244,174.84873367914838],[-37.80695645133336,174.85708513345912]]}
                    opacity={1.0}
                  />
                  {/*
                  <ImageOverlay
                    url="imageoverlay/C1.gif"
                    bounds={[[-37.8117, 174.8488], [-37.8069, 174.8571]]}
                    opacity={1.0}
                  />
                  */}
                  <ImageOverlay
                    url="imageoverlay/C2.gif"
                    bounds={[[-37.809705480267105,174.8565839767456],[-37.80485664765824,174.86510662078865]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/C3.gif"
                    bounds={[[-37.813826554893694,174.85655514877055],[-37.80934243746885,174.86534573232075]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/D1.gif"
                    bounds={[[-37.79893462199677,174.86723756904064],[-37.79391321789594,174.87561909066648]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/D2.gif"
                    bounds={[[-37.803155282927456,174.86671884040365],[-37.79862895198037,174.87518809369357]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/D3.gif"
                    bounds={[[-37.80780776271038,174.86460253447294],[-37.80292757720871,174.87292854040862]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/E1.gif"
                    bounds={[[-37.79878826435218,174.8751432721208],[-37.79405024787433,174.88355153944437]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/E2.gif"
                    bounds={[[-37.80324538654141,174.8747777938843],[-37.798262605492184,174.88307476043704]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/E3.gif"
                    bounds={[[-37.80741869345871,174.87246542254843],[-37.802895483493586,174.88097617596938]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/F1.gif"
                    bounds={[[-37.80291187939292,174.8825891711274],[-37.79833131499995,174.8912214028592]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/F2.gif"
                    bounds={[[-37.807771301525,174.88047004929254],[-37.80258107125249,174.88892554349405]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/G1.gif"
                    bounds={[[-37.804391793414936,174.88842621280702],[-37.79977272644145,174.8968578902558]]}
                    opacity={1.0}
                  />
                  <ImageOverlay
                    url="imageoverlay/G2.gif"
                    bounds={[[-37.80865625986052,174.8863036124433],[-37.80432610663525,174.89486519164018]]}
                    opacity={1.0}
                  />
                </LayerGroup>
              </Overlay>
              <Overlay checked name="Voronoi">
                {/* if a hydrant has been added the key value forces the layer to update */}
                <GeoJSON
                  data={voronoiPolygons}
                  style={voronoiStyle}
                  key={voronoiPolygons.features.length}
                />
              </Overlay>
              <Overlay checked name="Positions">
                <FeatureGroup>
                  {hydrants.map(h => <CircleMarker
                    center={[h.lat, h.lon]}
                    radius={5}
                    weight={2}
                    key={h.id}
                    color={hydrantColor(h)}
                  >
                    <Popup>
                      <div>
                        <table className="hydrant-popup">
                          <tbody>
                            <tr>
                              <td>Type</td>
                              <td>{niceType(h.type)}</td>
                            </tr>
                            <tr>
                              <td>Position</td>
                              <td>{nicePosition(h.position)}</td>
                            </tr>
                            <tr>
                              <td>Working</td>
                              <td>{niceWorking(h.working)}</td>
                            </tr>
                            <tr>
                              <td>Test date</td>
                              <td>{niceDate(h.testDate)}</td>
                            </tr>
                            <tr>
                              <td>Lon</td>
                              <td>{h.lon}</td>
                            </tr>
                            <tr>
                              <td>Lat</td>
                              <td>{h.lat}</td>
                            </tr>
                            <tr>
                              <td>Comment</td>
                              <td>{h.comment}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </Popup>
                  </CircleMarker>)}
                </FeatureGroup>
              </Overlay>
            </LayersControl>
            {coords && <CircleMarker
              center={[coords.latitude, coords.longitude]}
              radius={5}
              weight={3}
              color="#FF0000"
            />}
          </Map>
        </div>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    hydrants: state.hydrants,
    viewport: state.viewport,
  }
}
const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    loadHydrants: (bbox: string) => dispatch(loadHydrants(bbox)),
    setViewport: (viewport: Viewport) => dispatch(setViewport(viewport)),
    // addHydrant: () => {
    //   dispatch({
    //     type: 'LOAD_HYDRANTS_DONE', payload: { elements: [{lon: 174.871004, lat: -37.802}] }
    //   })
    // },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(geolocated({
  userDecisionTimeout: 10000,
  watchPosition: true,
  positionOptions: {
    enableHighAccuracy: true
  }
})(App))
