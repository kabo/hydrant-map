// @flow

/* eslint no-unused-vars: "off" */

import React from 'react'
import ReactDOM from 'react-dom'
import {createStore, compose} from 'redux'
import {Provider} from 'react-redux'
import { offline } from '@redux-offline/redux-offline'
import defaultOfflineConfig from '@redux-offline/redux-offline/lib/defaults' // flowlint-line untyped-import:off
import Amplify, { Logger } from 'aws-amplify'
import aws_exports from './aws-exports' // flowlint-line untyped-import:off
import reducer from './reducer'
import 'leaflet/dist/leaflet.css'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker' // flowlint-line untyped-import:off

Amplify.configure(aws_exports)
const logger = new Logger('index', 'INFO')
const offlineConfig = {
  ...defaultOfflineConfig,
  persistOptions: {
    whitelist: ['hydrants', 'viewport']
  },
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(reducer, composeEnhancers(offline(offlineConfig)))

const rootElement = document.getElementById('root')
if (rootElement) {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    rootElement)
  registerServiceWorker()
} else {
  console.error('No root element found!')
}
